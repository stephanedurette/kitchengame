using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrderManager : MonoBehaviour
{
    [SerializeField] private AvailableOrdersScriptableObject availableOrdersSO;
    [SerializeField] private List<DeliveryCounter> availableCounters;
    [SerializeField] private float timeBetweenOrders = 5f;

    private float currentTimeBetweenOrders = 0f;
    private int numOrders = 0;

    public static OrderManager Instance;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        } else
        {
            Destroy(this.gameObject);
        }
    }

    private void Start()
    {
        currentTimeBetweenOrders = 0f;
        numOrders = 0;
    }

    private void Update()
    {
        if (numOrders >= availableCounters.Count) return;

        currentTimeBetweenOrders += Time.deltaTime;

        if (currentTimeBetweenOrders > timeBetweenOrders)
        {
            currentTimeBetweenOrders = 0f;

            OrderRecipeScriptableObject randomRecipe = availableOrdersSO.AvailableRecipes[Random.Range(0, availableOrdersSO.AvailableRecipes.Count - 1)];

            int randomCounterIndex = Random.Range(0, availableCounters.Count - 1);

            while (availableCounters[randomCounterIndex].OrderRecipe != null) randomCounterIndex = (randomCounterIndex + 1) % availableCounters.Count;

            availableCounters[randomCounterIndex].SetOrderRecipe(randomRecipe);

            numOrders++;
        }
    }
}
