using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlateCounter : Counter
{
    [SerializeField] KitchenObjectScriptableObject plateCounterScriptableObject;
    [SerializeField] private int maxPlates;
    [SerializeField] private Vector3 plateSpawnOffset;
    [SerializeField] private float plateSpawnTime = 4f;

    private List<KitchenObject> plates;

    private float currentSpawnTime;

    public void Start()
    {
        currentSpawnTime = 0f;
        plates = new List<KitchenObject>();
    }

    private void Update()
    {
        if (plates.Count >= maxPlates) return;

        currentSpawnTime += Time.deltaTime;

        if (currentSpawnTime > plateSpawnTime)
        {
            plates.Add(Instantiate(plateCounterScriptableObject.prefab, counterTopPosition.position + plates.Count * plateSpawnOffset, Quaternion.identity, transform).GetComponent<KitchenObject>());
            currentSpawnTime = 0f;
        }
    }

    public override void Interact(Player player)
    {
        {
            if (plates.Count > 0 && player.KitchenObject == null)
            {
                KitchenObject.SpawnKitchenObject(plateCounterScriptableObject, player);
                GameObject.Destroy(plates[plates.Count - 1].gameObject);
                plates.RemoveAt(plates.Count - 1);
            }
        }
    }
}
