using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClearCounter : Counter, IKitchenObjectHolder
{

    private KitchenObject kitchenObject;

    public KitchenObject KitchenObject { get { return kitchenObject; } set { kitchenObject = value; } }

    public Transform KitchenObjectPosition => counterTopPosition;

    public override void Interact(Player player)
    {
        //take player's kitchen object
        if (kitchenObject == null)
        {
            player.KitchenObject?.SetKitchenObjectHolder(this);
        } 
        // put player's kitchen object on our plate
        else if (kitchenObject.IsPlate(out Plate plate) && player.KitchenObject != null)
        {
            player.KitchenObject.SetPlate(plate);

        } 
        // put our kitchen object on player's plate
        else if (player.KitchenObject != null && player.KitchenObject.IsPlate(out plate))
        {
            kitchenObject?.SetPlate(plate);
        } 
        // give player our kitchen object
        else
        {
            kitchenObject.SetKitchenObjectHolder(player);
        }
    }
}
