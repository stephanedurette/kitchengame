using UnityEngine;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/OrderRecipeScriptableObject", order = 4)]
public class OrderRecipeScriptableObject : ScriptableObject
{
    public List<KitchenObjectScriptableObject> Ingedients;
    public string Name;
}
