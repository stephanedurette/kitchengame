using UnityEngine;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/AvailableOrdersScriptableObject", order = 5)]
public class AvailableOrdersScriptableObject : ScriptableObject
{
    public List<OrderRecipeScriptableObject> AvailableRecipes;
}
