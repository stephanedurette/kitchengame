using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/KitchenObjectScriptableObject", order = 1)]
public class KitchenObjectScriptableObject : ScriptableObject
{
    public string objectName;
    public Sprite sprite;
    public Transform prefab;
}
