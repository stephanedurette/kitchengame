using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FryingCounter : Counter, IKitchenObjectHolder
{
    [SerializeField] private FryingRecipeScriptableObject[] fryingRecipeSO;
    [SerializeField] private ProgressBarUI progressBarUI;

    private KitchenObject kitchenObject;
    private KitchenObject fryingResultObject;

    private float timeToFry;
    private float currentFryingTime;

    public KitchenObject KitchenObject { get { return kitchenObject; } set { kitchenObject = value; } }

    public Transform KitchenObjectPosition => counterTopPosition;

    private void Start()
    {
        progressBarUI.Show(false);
    }

    private void Update()
    {
        if (fryingResultObject == null) return;

        currentFryingTime += Time.deltaTime;
        progressBarUI.UpdateProgressBar(currentFryingTime, timeToFry);

        if (currentFryingTime >= timeToFry)
        {
            kitchenObject.Destroy();
            KitchenObject.SpawnKitchenObject(fryingResultObject.KitchenObjectSO, this);

            currentFryingTime = 0f;
            fryingResultObject = FryResult(kitchenObject, out timeToFry);
            progressBarUI.Show(fryingResultObject != null);
        }
    }

    public override void Interact(Player player)
    {
        if (kitchenObject == null)
        {
            if (player.KitchenObject == null) return;

            fryingResultObject = FryResult(player.KitchenObject, out timeToFry);
            if (fryingResultObject == null) return;

            player.KitchenObject.SetKitchenObjectHolder(this);

            currentFryingTime = 0f;
            progressBarUI.Show(fryingResultObject != null);
        } else if(player.KitchenObject != null && player.KitchenObject.IsPlate(out Plate plate))
        {
            kitchenObject.SetPlate(plate);
            fryingResultObject = null;
            progressBarUI.Show(false);
        } else
        {
            kitchenObject.SetKitchenObjectHolder(player);
            fryingResultObject = null;
            progressBarUI.Show(false);
        }
    }

    public KitchenObject FryResult(KitchenObject kitchenObject, out float time)
    {
        foreach (var c in fryingRecipeSO)
        {
            if (c.startObject != kitchenObject.KitchenObjectSO) continue;

            time = c.time;
            return c.endObject.prefab.GetComponent<KitchenObject>();
        }

        time = 0f;
        return null;
    }
}
