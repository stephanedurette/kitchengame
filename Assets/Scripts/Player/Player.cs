using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class Player : MonoBehaviour, IKitchenObjectHolder
{
    [SerializeField] private float speed = 0.7f;
    [SerializeField] private float runSpeed = 0.7f;
    [SerializeField] private float rotateSpeed = 10f;
    [SerializeField] private LayerMask counterLayerMask;
    [SerializeField] private Transform itemHoldPosition;

    public static event EventHandler<OnCounterHighlightedEventArgs> OnCounterHighlighted;
    public class OnCounterHighlightedEventArgs : EventArgs
    {
        public Counter selectedCounter;
    }

    Counter highlightedCounter;
    KitchenObject kitchenObject;

    private bool isWalking;

    private Vector3 faceDirection;

    public bool IsWalking => isWalking;

    public Transform KitchenObjectPosition => itemHoldPosition;

    public KitchenObject KitchenObject { get { return kitchenObject; } set { kitchenObject = value; } }

    private void OnEnable()
    {
        InputManager.OnInteractAction += HandleInteraction;
        InputManager.OnAlternateInteractAction += HandleAlternateInteraction;
    }

    private void OnDisable()
    {
        InputManager.OnInteractAction -= HandleInteraction;
        InputManager.OnAlternateInteractAction += HandleAlternateInteraction;
    }

    private void Update()
    {
        HandleMovement();
        UpdateHighlightedCounter();
    }

    private void HandleInteraction(object sender, EventArgs args)
    {
        highlightedCounter?.Interact(this);
    }
    private void HandleAlternateInteraction(object sender, EventArgs args)
    {
        highlightedCounter?.AlternateInteract(this);
    }

    private void UpdateHighlightedCounter()
    {
        float maxInteractDistance = .5f;

        highlightedCounter = null;

        if (Physics.Raycast(transform.position, faceDirection, out RaycastHit hitInfo, maxInteractDistance, counterLayerMask))
        {
            hitInfo.collider.gameObject.TryGetComponent(out highlightedCounter);
        }

        OnCounterHighlightedEventArgs eventArgs = new OnCounterHighlightedEventArgs { selectedCounter = highlightedCounter };
        OnCounterHighlighted?.Invoke(this, eventArgs);
    }

    private void HandleMovement()
    {
        Vector2 inputVector = InputManager.Instance.GetInputVector();
        Vector3 moveDirection = new Vector3(inputVector.x, 0, inputVector.y);

        faceDirection = moveDirection != Vector3.zero ? moveDirection : faceDirection;

        float moveSpeed = InputManager.Instance.IsSprintPressed() ? runSpeed : speed;

        float moveDistance = moveSpeed * Time.deltaTime;

        bool canMove = CanMoveInDirection(moveDirection, moveDistance);
        bool canMoveForwardBackward = CanMoveInDirection(moveDirection.z * Vector3.forward, moveDistance);
        bool canMoveLeftRight = CanMoveInDirection(moveDirection.x * Vector3.right, moveDistance);

        //Debug.Log($"CanMove: {canMove}\t\tcanMoveForwardBackward: {canMoveForwardBackward}\t\tcanMoveLeftRight: {canMoveLeftRight}");

        transform.forward = Vector3.Slerp(transform.forward, moveDirection, Time.deltaTime * rotateSpeed);

        Vector3 afterCollisionMoveDirection = moveDirection;

        if (!canMove) afterCollisionMoveDirection = Vector3.zero;
        if (!canMove && canMoveForwardBackward) afterCollisionMoveDirection = moveDirection.z * Vector3.forward;
        if (!canMove && canMoveLeftRight) afterCollisionMoveDirection = moveDirection.x * Vector3.right;

        

        isWalking = afterCollisionMoveDirection != Vector3.zero;

        transform.position += afterCollisionMoveDirection.normalized * moveDistance;
    }

    private bool CanMoveInDirection(Vector3 moveDirection, float moveDistance)
    {
        float playerRadius = .25f;
        return !Physics.CapsuleCast(transform.position + Vector3.down * 2 * playerRadius, transform.position + Vector3.up * 2 * playerRadius, playerRadius, moveDirection, moveDistance);
    }

}
