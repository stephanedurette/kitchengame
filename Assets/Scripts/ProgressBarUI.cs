using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBarUI : MonoBehaviour
{
    [SerializeField] private Image fillImage, backgroundImage;

    public void Show(bool show)
    {
        fillImage.enabled = show;
        backgroundImage.enabled = show;
    }

    public void UpdateProgressBar(float amount, float maxAmount)
    {
        if (maxAmount == 0) return;

        fillImage.fillAmount = amount / maxAmount;
    }
}
