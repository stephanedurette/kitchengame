using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KitchenObject : MonoBehaviour
{
    [SerializeField] private KitchenObjectScriptableObject kitchenObjectSO;
    public KitchenObjectScriptableObject KitchenObjectSO => kitchenObjectSO;

    private IKitchenObjectHolder kitchenObjectHolder;

    public void SetKitchenObjectHolder(IKitchenObjectHolder kitchenObjectHolder)
    {
        if (kitchenObjectHolder.KitchenObject != null) return;

        if (this.kitchenObjectHolder != null)
            this.kitchenObjectHolder.KitchenObject = null;

        this.kitchenObjectHolder = kitchenObjectHolder;
        kitchenObjectHolder.KitchenObject = this;

        this.transform.parent = kitchenObjectHolder.KitchenObjectPosition;
        transform.localPosition = Vector3.zero;
    }

    public void SetPlate(Plate plate)
    {
        plate.AddIngredient(this.kitchenObjectSO);
        Destroy();
    }

    public void Destroy()
    {
        kitchenObjectHolder.KitchenObject = null;
        Destroy(this.gameObject);
    }

    public static void SpawnKitchenObject(KitchenObjectScriptableObject kitchenObjectSO, IKitchenObjectHolder holder)
    {
        Instantiate(kitchenObjectSO.prefab).GetComponent<KitchenObject>().SetKitchenObjectHolder(holder);
    }

    public virtual bool IsPlate(out Plate plate)
    {
        plate = null;
        return false;
    }
}
