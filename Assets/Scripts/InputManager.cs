using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InputManager : MonoBehaviour
{
    public static InputManager Instance;

    private PlayerInputActions playerInputActions;

    public static event EventHandler OnInteractAction;
    public static event EventHandler OnAlternateInteractAction;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        } else
        {
            Destroy(this.gameObject);
        }
    }

    private void OnEnable()
    {
        playerInputActions = new PlayerInputActions();
        playerInputActions.Player.Enable();

        playerInputActions.Player.Interact.performed += InteractPerformed;
        playerInputActions.Player.AlternateInteract.performed += AlternateInteractPerformed;
    }

    public bool IsSprintPressed()
    {
        return playerInputActions.Player.Sprint.IsPressed();
    }

    private void OnDisable()
    {
        playerInputActions.Player.Interact.performed -= InteractPerformed;
        playerInputActions.Player.AlternateInteract.performed -= AlternateInteractPerformed;
    }

    private void AlternateInteractPerformed(UnityEngine.InputSystem.InputAction.CallbackContext obj)
    {
        OnAlternateInteractAction?.Invoke(this, EventArgs.Empty);
    }

    private void InteractPerformed(UnityEngine.InputSystem.InputAction.CallbackContext obj)
    {
        OnInteractAction?.Invoke(this, EventArgs.Empty);
    }

    public Vector2 GetInputVector()
    {
        Vector2 inputVector = playerInputActions.Player.Move.ReadValue<Vector2>();

        return inputVector;
    }
}
