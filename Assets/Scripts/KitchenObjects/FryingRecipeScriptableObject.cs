using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/FryingRecipeScriptableObject", order = 3)]
public class FryingRecipeScriptableObject : ScriptableObject
{
    public KitchenObjectScriptableObject startObject;
    public KitchenObjectScriptableObject endObject;
    public float time;
}
