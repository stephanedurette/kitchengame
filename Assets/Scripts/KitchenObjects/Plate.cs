using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plate : KitchenObject
{
    [SerializeField] private Vector3 ingredientOffset = Vector3.up * 0.01f;

    private List<KitchenObjectScriptableObject> ingredients;

    public List<KitchenObjectScriptableObject> Ingredients => ingredients;

    private void Start()
    {
        ingredients = new List<KitchenObjectScriptableObject>();   
    }

    public void AddIngredient(KitchenObjectScriptableObject kitchenScriptableObject)
    {
        Vector3 spawnPosition = transform.position + ingredientOffset * (ingredients.Count + 1);
        var ingredient = Instantiate(kitchenScriptableObject.prefab, spawnPosition, Quaternion.identity, transform).GetComponent<KitchenObject>().KitchenObjectSO;
        ingredients.Add(ingredient);
    }

    public override bool IsPlate(out Plate plate)
    {
        plate = this;
        return true;
    }
}
