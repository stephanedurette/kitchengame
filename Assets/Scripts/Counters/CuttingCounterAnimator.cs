using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class CuttingCounterAnimator : MonoBehaviour
{
    private Animator animator;
    [SerializeField] private CuttingCounter cuttingCounter;
    private int cutAnimationHash = Animator.StringToHash("Cutting");

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void Cut()
    {
        animator.SetTrigger(cutAnimationHash);
    }

    public void OnFoodCut()
    {
        cuttingCounter.SpawnCutFood();
    }
}
