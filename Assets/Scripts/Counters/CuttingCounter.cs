using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CuttingCounter : Counter, IKitchenObjectHolder
{
    [SerializeField] private CuttingCounterAnimator cuttingCounterAnimator;
    [SerializeField] private CuttingRecipeScriptableObject[] cuttingRecipeScriptableObjects;

    private KitchenObject kitchenObject;

    private KitchenObject cutResult;

    public KitchenObject KitchenObject { get { return kitchenObject; } set { kitchenObject = value; } }

    public Transform KitchenObjectPosition => counterTopPosition;

    public override void Interact(Player player)
    {
        if (kitchenObject == null)
        {
            player.KitchenObject?.SetKitchenObjectHolder(this);
        }
        else if (player.KitchenObject != null && player.KitchenObject.IsPlate(out Plate plate))
        {
            kitchenObject?.SetPlate(plate);
            cutResult = null;
        }
        else
        {
            kitchenObject.SetKitchenObjectHolder(player);
            cutResult = null;
        }
    }

    public override void AlternateInteract(Player player)
    {
        if (kitchenObject == null) return;

        cutResult = CutResult(kitchenObject);

        if (cutResult == null) return;
        
        cuttingCounterAnimator.Cut();
    }

    public void SpawnCutFood()
    {
        kitchenObject?.Destroy();
        if (cutResult == null) return;
        KitchenObject.SpawnKitchenObject(cutResult.KitchenObjectSO, this);
    }

    public KitchenObject CutResult(KitchenObject kitchenObject)
    {
        foreach (var c in cuttingRecipeScriptableObjects)
        {
            if (c.startObject != kitchenObject.KitchenObjectSO) continue;

            return c.endObject.prefab.GetComponent<KitchenObject>();
        }

        return null;
    }
}
