using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodBoxCounter : Counter
{
    [SerializeField] private KitchenObjectScriptableObject kitchenObjectSO;
    [SerializeField] private FoodBoxCounterAnimator foodBoxCounterAnimator;

    private Player player;

    public override void Interact(Player player)
    {
        if (player.KitchenObject == null)
        {
            this.player = player;
            foodBoxCounterAnimator.OpenBox();
        }
    }

    public void SpawnFoodForPlayer()
    {
        KitchenObject.SpawnKitchenObject(kitchenObjectSO, player);
    }
}
