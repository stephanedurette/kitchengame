using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class FoodBoxCounterAnimator : MonoBehaviour
{
    private Animator animator;
    [SerializeField] private FoodBoxCounter foodBoxCounter;
    private int openAnimationHash = Animator.StringToHash("Opened");

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void OpenBox()
    {
        animator.SetTrigger(openAnimationHash);
    }

    public void OnBoxOpened()
    {
        foodBoxCounter.SpawnFoodForPlayer();
    }
}
