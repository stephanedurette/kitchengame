using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeliveryCounter : Counter, IKitchenObjectHolder
{
    [SerializeField] private MeshRenderer counterTopMesh;

    private KitchenObject kitchenObject;

    public KitchenObject KitchenObject { get { return kitchenObject; } set { kitchenObject = value; } }

    private OrderRecipeScriptableObject orderRecipe;

    public OrderRecipeScriptableObject OrderRecipe => orderRecipe;

    public Transform KitchenObjectPosition => counterTopPosition;

    private void Start()
    {
        ToggleConveyerBelt(false);
    }

    public override void Interact(Player player)
    {
        Plate plate;

        if (kitchenObject == null && player.KitchenObject != null && player.KitchenObject.IsPlate(out plate))
        {
            player.KitchenObject?.SetKitchenObjectHolder(this);

            if (RecipeComplete(plate)){
                Debug.Log("recipe complete");
            }
        }
        //take player's kitchen object
        else if (kitchenObject == null)
        {
            player.KitchenObject?.SetKitchenObjectHolder(this);
        } 
        // put player's kitchen object on our plate
        else if (kitchenObject.IsPlate(out plate) && player.KitchenObject != null)
        {
            player.KitchenObject.SetPlate(plate);

            if (RecipeComplete(plate))
            {
                Debug.Log("recipe complete");
            }

        } 
        // put our kitchen object on player's plate
        else if (player.KitchenObject != null && player.KitchenObject.IsPlate(out plate))
        {
            kitchenObject?.SetPlate(plate);
        } 
        // give player our kitchen object
        else
        {
            kitchenObject.SetKitchenObjectHolder(player);
        }
    }

    private void ToggleConveyerBelt(bool on)
    {
        float moveSpeed = -0.5f;

        counterTopMesh.material.SetFloat("_Speed", on ? moveSpeed : 0f);
    }

    public void SetOrderRecipe(OrderRecipeScriptableObject orderRecipeSO)
    {
        orderRecipe = orderRecipeSO;
        ToggleConveyerBelt(true);
    }

    public bool RecipeComplete(Plate plate)
    {
        if (plate.Ingredients.Count != orderRecipe.Ingedients.Count) return false;

        for(int i = 0; i < plate.Ingredients.Count; i++)
        {
            if (plate.Ingredients[i] != orderRecipe.Ingedients[i]) return false;
        }

        return true;
    }
}
