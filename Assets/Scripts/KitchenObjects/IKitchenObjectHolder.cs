using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IKitchenObjectHolder
{
    public Transform KitchenObjectPosition { get; }
    public KitchenObject KitchenObject { get; set; }
}
