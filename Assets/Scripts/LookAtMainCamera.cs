using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtMainCamera : MonoBehaviour
{
    [SerializeField] bool inverted = false;

    private Transform transform;

    // Start is called before the first frame update
    void Start()
    {
        transform = GetComponent<Transform>();
    }

    // Update is called once per frame
    void LateUpdate()
    {
        transform.forward = inverted ? -Camera.main.transform.forward : Camera.main.transform.forward;
    }
}
