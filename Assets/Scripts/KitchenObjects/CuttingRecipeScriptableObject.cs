using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/CuttingRecipeScriptableObject", order = 2)]
public class CuttingRecipeScriptableObject : ScriptableObject
{
    public KitchenObjectScriptableObject startObject;
    public KitchenObjectScriptableObject endObject;
}
