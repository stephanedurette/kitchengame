using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CounterHighlight : MonoBehaviour
{
    [SerializeField] private Material highlightedMaterial;
    [SerializeField] private Material defaultMaterial;

    public void SetHighlightedMaterial(bool highlighted)
    {
        Material appliedMaterial = highlighted ? highlightedMaterial : defaultMaterial;

        foreach (MeshRenderer m in GetComponentsInChildren<MeshRenderer>())
        {
            m.material = appliedMaterial;
        }
    }

}
