using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Counter : MonoBehaviour
{
    private CounterHighlight counterHighight;
    [SerializeField] private protected Transform counterTopPosition;

    public abstract void Interact(Player player);

    public virtual void AlternateInteract(Player player)
    {
        Debug.Log("no alternate interaction");
    }

    private void OnEnable()
    {
        Player.OnCounterHighlighted += OnCounterHighlighted;
    }

    private void OnDisable()
    {
        Player.OnCounterHighlighted -= OnCounterHighlighted;
    }

    private void Awake()
    {
        counterHighight = GetComponentInChildren<CounterHighlight>();
    }

    private void OnCounterHighlighted(object sender, Player.OnCounterHighlightedEventArgs e)
    {
        counterHighight.SetHighlightedMaterial(e.selectedCounter == this);
    }
}
